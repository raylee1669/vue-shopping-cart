module.exports = {

    devServer: {
        open: true,
        host: 'localhost',
        port: 8080,
        https: false,
        hotOnly: false,
        proxy: { // 配置跨域
            '/baseurl': {
                target: 'http://shop.ray-lee.name/oct/',
                ws: true,
                changOrigin: true,
                pathRewrite: {
                    '^/baseurl': ''
                }
            }
        },
        before: app => { }
    }
}
