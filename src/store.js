import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

var car = JSON.parse(localStorage.getItem('car') || '[]');

export default new Vuex.Store({
  state: {
    car: car
  },
  mutations: {
    CartList(state, goodinfo) {
      var flag = false
      state.car.some(item => {
        if (item.id == goodinfo.id) {
          item.count += goodinfo.count
          flag = true
          return true
        }
      })
      if (!flag) {
        state.car.push(goodinfo)
      }
      localStorage.setItem('car', JSON.stringify(state.car));
    },
    Cartcount(state, goodinfo) {
      state.car.some(item => {
        if (item.id == goodinfo.id & item.count > 1) {
          item.count += goodinfo.count
          return false
        }
      })
    },
    Cartmove(state, id) {
      state.car.some((item, i) => {
        if (item.id == id) {
          state.car.splice(i, 1)
          return true
        }
      })
      localStorage.setItem('car', JSON.stringify(state.car));
    }
  },
  getters: {
    Countall(state) {
      var c = 0;
      state.car.forEach(item => {
        c += item.count;
      })
      return c
    },
    Totalprice(state) {
      var t = 0;
      state.car.forEach(item => {
        t += (item.count * item.price);
      })
      return t
    }
  }
  ,
  actions: {

  }
})
